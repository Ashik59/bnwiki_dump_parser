**datafinder.py** is a script that downloads wikipedia latest dumps, extracts xml files from xml.bz2 and converts the xml file to json.

**indexingwikidump** is used to index infos("url"."title","id","content","infobox","domain","tags") parsed from wiki dumps into **Solr**



**bnwiki_dump_parser** is a parser for bengali wikipedia. It parses id,title,content and infobox and writes it in a csv file

**Steps:**
- run datafinder.py
- for indexing run indexingwikidump.py
- for writing in csv run bnwiki_dump_parser



# coding: utf-8

# # Imports


import pandas as pd
import xmltodict
import json
import csv
import wptools as wp
import pysolr as ps
import hashlib


# # loading data



# jsonFile = 'bnwiki-20200620-pages-articles.json'
jsonFile = 'bnwiki-latest-pages-articles.json'





df = pd.read_json(jsonFile)
print(df)


# # parsing



data = df['mediawiki']['page']




all_titles = []
all_id = []
all_revision = []

for x in data:
    all_titles.append(x['title'])
    all_id.append(x['id'])
    all_revision.append(x['revision']['text'])

    
all_titles = [w.replace(' ', '_') for w in all_titles]

# assert(len(all_titles) == len(all_id) and len(all_id) == len(all_revision) and len(all_id) == 403733)



def getInfoBox(title):  
    ret = dict()
    try:
        page = wp.page(title, silent=True, lang='bn')
        page.get_parse()
        ret = page.data['infobox']
        
        if(ret == None):
            ret = {"No infobox found"}
    except:
        ret = {}
    finally:
        return ret


# # Indexing to Solr


total_len = len(all_id)


solr = ps.Solr('http://dev.pipilika.com:5551/solr/infobox', always_commit=True,)


for i in range(total_len):
    if('টেমপ্লেট' in all_titles[i] or 'মিডিয়াউইকি' in all_titles[i] or 'উইকিপিডিয়া' in all_titles[i] or'Template' in all_titles[i] or 'template' in all_titles[i]):
        continue;
    if('#text' in all_revision[i]):
        result= "https://bn.wikipedia.org/wiki/" + all_titles[i]
        data = {
            "url": result,
            "title": all_titles[i],
            "content": all_revision[i]['#text'],
            "infobox": getInfoBox(all_titles[i]),
            "domain": "bn.wikipedia.org",
            "has_infobox": 'true',
            "tags": all_id[i],
            "url_md5": (hashlib.md5(result.encode()).hexdigest()),
            "signature_key_title": result
        }
        try:
            solr.add(docs=[data], commit=True, overwrite=False)
        except:
            print("This data is not indexed: \n", data)
        
    else:
        result= "https://bn.wikipedia.org/wiki/" + all_titles[i]
        data = {
            "url": result,
            "title": all_titles[i],
            "content": '',
            "infobox": getInfoBox(all_titles[i]),
            "domain": "bn.wikipedia.org",
            "has_infobox": 'true',
            "tags": all_id[i],
            "url_md5": (hashlib.md5(result.encode()).hexdigest()),
            "signature_key_title": result
        }
        try:
            solr.add(docs=[data], commit=True, overwrite=False)
        except:
            print("This data is not indexed: \n", data)
    
    


# # Delete all entries from solr 
# solr.delete(q='*:*')



import pandas as pd
import xmltodict
import json
import urllib.request 
import tarfile

print("downloading xml.bz2")
urllib.request.urlretrieve("https://dumps.wikimedia.org/bnwiki/latest/bnwiki-latest-pages-articles.xml.bz2")

print("extracting bz2")
tar = tarfile.open("bnwiki-latest-pages-articles.xml.bz2", "r:bz2")  
for i in tar:
  tar.extractfile(i)
tar.close()


print("converting xml to json")
xmlFile = 'bnwiki-latest-pages-articles.xml'
jsonFile = 'bnwiki-latest-pages-articles.json'

data_dict = {}

with open(xmlFile) as xml_file:       
    data_dict = xmltodict.parse(xml_file.read()) 
    xml_file.close() 

json_data = json.dumps(data_dict) 

with open(jsonFile, "w") as json_file: 
    json_file.write(json_data) 
    json_file.close()
print("process complete")


# coding: utf-8

# # Import

import pandas as pd
import xmltodict
import json
import csv
import wptools as wp


# # File Names


xmlFile = 'bnwiki-20200620-pages-articles.xml'
jsonFile = 'bnwiki-20200620-pages-articles.json'
csvFile = 'bnwiki-20200620-pages-articles.csv'


# # Create JSON File


data_dict = {}

with open(xmlFile) as xml_file:       
    data_dict = xmltodict.parse(xml_file.read()) 
    xml_file.close() 

json_data = json.dumps(data_dict) 

with open(jsonFile, "w") as json_file: 
    json_file.write(json_data) 
    json_file.close()


# # Create DataFrame


df = pd.read_json(jsonFile)
print(df)


# # Get Data


data = df['mediawiki']['page']




all_titles = []
all_id = []
all_revision = []

for x in data:
    all_titles.append(x['title'])
    all_id.append(x['id'])
    all_revision.append(x['revision']['text'])

assert(len(all_titles) == len(all_id) and len(all_id) == len(all_revision) and len(all_id) == 403733)


# # Create CSV


total_len = len(all_id)




def getInfoBox(title):  
    ret = dict()
    try:
        page = wp.page(title, silent=True, lang='bn')
        page.get_parse()
        ret = page.data['infobox']
        
        if(ret == None):
            ret = {"No infobox found"}
    except:
        ret = {}
    finally:
        return ret



with open(csvFile, 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["Title", "ID", "Revision", "InfoBox"])
    for i in range(total_len):
        if('টেমপ্লেট' in all_titles[i] or 'Template' in all_titles[i] or 'template' in all_titles[i]):
            continue;
        if('#text' in all_revision[i]):
            newList = [all_titles[i], all_id[i], all_revision[i]['#text'], getInfoBox(all_titles[i])]
        else:
            newList = [all_titles[i], all_id[i], '', getInfoBox(all_titles[i])]
        writer.writerow(newList)


# # Check CSV



newDf = pd.read_csv(csvFile)
newDf.tail(100)

